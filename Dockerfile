FROM debian:9 as build

RUN apt update && apt install -y wget gcc make build-essential libssl1.0-dev libpcre3 libpcre3-dev zlib1g-dev 
RUN wget http://nginx.org/download/nginx-1.0.5.tar.gz && tar xvfz nginx-1.0.5.tar.gz && \
cd nginx-1.0.5 &&  ./configure --with-http_ssl_module  --with-pcre && make && make install

FROM debian:9 
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
#Sdelal kak mudak
COPY --from=build /usr/local/lib/ /usr/lib/
COPY --from=build /usr/local/nginx/conf/nginx.conf /etc/nginx/nginx.conf
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"] 

